/* mithrilx.js: extensions for Mithril.js to keep CSS close to component
 *      definitions.
 * Copyright Joshua D. Bartlett
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import m from "./libs/mithril.js"
import { addCssRules } from "./css.js"

const existingComponents = {}


/* component([base], selector, details): defines a mithril component
 *
 * This helper function can be used to define a Mithril component with
 * associated CSS styling. Arguments are:
 *
 * base (optional): a component to extend. When extending another component,
 *      the given selector must be a class name (e.g., '.bold'), and the
 *      resulting Mithril virtual dom node will be given both the base
 *      component's class and the extension component's class. The view
 *      function of the extension component will be called first, and its
 *      result will be used to build the parameters to pass to m(Base, ...).
 *      This means that the base component's view function must be able to
 *      accept a 'class' key in vnode.attrs. The `addClass()` function
 *      can be helpful for this purpose.
 *
 * selector: a string indicating the component's selector, e.g., 'span.message'.
 *      This is passed on to Mithril's m() function, and is also used to add
 *      CSS styling (if any).
 *
 * details: an object whose contents will be passed on as attrs to Mithril's
 *      m() function, with the following modifications:
 *
 * details.css (optional: if this is a string, it will be added as CSS for this
 *      component's selector. If it is an object, each entry in the object will
 *      be added as CSS. E.g.,
 *          const MyLink = component('a.my-link', {
 *              css: {
 *                  '': 'color: white',         // For a.my-link
 *                  ':hover': 'color: red',     // For a.my-link:hover
 *              }
 *
 * details.mobileCss (optional): like details.css, but this will be added
 *      with a media selector so that it only applies to media with
 *      max-width: 32rem.
 *
 * details.printCss (optional): like details.css, but this will be added
 *      with a media selector so that it only applies to print media.
 *
 * details.view (optional): a function that either returns a list of children
 *      or an object of attrs that will be passed to Mithril's m() function.
 *      To provide both children and attrs, return an attr object with a key
 *      'children' mapping to the children. If no 'children' key is returned
 *      in an attr dict, the children passed to the component will be passed
 *      on directly. If details.view is not provided, both attrs and children
 *      will be passed from the caller on to the m() function.
 *
 * details.removeIfEmpty (optional): if true, this component will not render
 *      at all if it has not children.
 */
export function component(arg1, arg2, arg3) {
    let base = null, selector, details, extensionClass = null
    let result
    if (typeof arg1 === 'string') {
        selector = arg1
        details = arg2 || {}
        result = {}
    } else {
        if (arg2[0] !== '.') {
            throw new Error('Components extending a base must have a selector that begins with "."')
        }
        extensionClass = arg2.slice(1)
        base = arg1
        selector = `${base.selector}.${extensionClass}`
        details = arg3 || {}
        result = Object.create(base)
    }

    // Extract and apply CSS rules
    const { css, mobileCss, printCss, removeIfEmpty, ...otherAttrs } = details
    Object.assign(result, otherAttrs)
    addCssRules(selector, css, mobileCss, printCss)

    // Ensure we don't accidentally use the same class name twice
    if (existingComponents[selector]) {
        throw new Error(`Component ${selector} is already defined elsewhere`)
    }
    existingComponents[selector] = true

    // Store selector for use when extending this component
    result.selector = selector

    // Define view function
    result.view = buildViewFunction(otherAttrs.view, base || selector, extensionClass, removeIfEmpty)

    return result
}


function buildViewFunction(viewFunction, selector, extensionClass, removeIfEmpty) {
    return function viewWrapper(vnode) {
        let m_attrs, m_children
        if (!viewFunction) {
            m_attrs = vnode.attrs
            m_children = vnode.children
        } else {
            const returnValue = viewFunction.call(vnode.state, vnode)
            if (Array.isArray(returnValue) || returnValue === null || returnValue === undefined) {
                m_attrs = {}
                m_children = returnValue
            } else if (typeof returnValue == 'string' || (returnValue?.tag && returnValue?.attrs && returnValue?.children)) {
                m_attrs = {}
                m_children = [returnValue]
            } else {
                const { children, ...attrs } = returnValue
                m_attrs = attrs
                m_children = children === undefined ? vnode.children : children
            }
        }

        if (removeIfEmpty && (!m_children || Array.isArray(m_children) && m_children.length === 0)) {
            return null
        }

        if (vnode.attrs?.class && m_attrs.class === undefined) {
            m_attrs.class = vnode.attrs.class
        }
        if (extensionClass) {
            m_attrs.class = m_attrs.class ? `${m_attrs.class} ${extensionClass}` : extensionClass
        }
        return m(selector, m_attrs, m_children)
    }
}


export function addClass(vnode, ...additionalClasses) {
    additionalClasses.unshift(vnode.attrs.class)
    additionalClasses = additionalClasses.filter(value => value !== undefined)
    if (additionalClasses.length === 0) {
        return vnode.attrs.class
    }
    return additionalClasses.join(' ')
}
