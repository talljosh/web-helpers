# [Web helpers](https://gitlab.com/talljosh/web-helpers)

This repository exists for Josh Bartlett to keep track of useful helpers and templates for modern web development.

At present, it contains:
 - `index.html`: drop-in html file that shows a loading screen while loading and running `main.js`
 - `loaders.js`: some helpers for loading CSS, JS, and webfonts, and adding `<link>` tags
 - `css.js`: helpers for adding to CSS styles from JS. Used by `mithrilx.js`.
 - `mithrilx.js`: extensions for `mithril.js` to allow defining CSS alongside the component it applies to
