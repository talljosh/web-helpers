/* css.js: helper functions for maintaining a set of generated style sheets
 * Copyright Joshua D. Bartlett
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


const css = new CSSStyleSheet()
document.adoptedStyleSheets.push(css)

const mobileCss = new CSSStyleSheet({media: '(max-width: 32rem)'})
document.adoptedStyleSheets.push(mobileCss)

const printCss = new CSSStyleSheet({media: 'print'})
document.adoptedStyleSheets.push(printCss)


export function addCssRule(rule) {
    css.insertRule(rule)
}

export function addMobileCssRule(rule) {
    mobileCss.insertRule(rule)
}

export function addPrintCssRule(rule) {
    printCss.insertRule(rule)
}

export function addCssRules(selector, standardRules, mobileRules, printRules) {
    addRules(selector, css, standardRules)
    addRules(selector, mobileCss, mobileRules)
    addRules(selector, printCss, printRules)
}


function addRules(selector, collection, rules) {
    switch (typeof rules) {
        case 'string':
            collection.insertRule(`${selector} { ${rules} }`)
            break
        case 'object':
            for (const extra in rules) {
                collection.insertRule(`${selector}${extra} { ${rules[extra]} }`)
            }
            break
    }
}
