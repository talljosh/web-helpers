/* loaders.js: Convenience methods for use in main.js
 * Copyright Joshua D. Bartlett
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


export async function createDomNodeFromUrl(url, tagName) {
    const response = await fetch(url)
    if (!response.ok) {
        throw new Error(`Status ${response.status} while loading ${url}`)
    }
    const element = document.createElement(tagName)
    element.appendChild(document.createTextNode(await response.text()))
    return element
}


export async function loadCss(url) {
    const cssElement = await createDomNodeFromUrl(url, 'style')
    document.head.appendChild(cssElement)
    return cssElement
}


export async function runJsScript(url) {
    const scriptElement = await createDomNodeFromUrl(url, 'script')
    document.body.appendChild(scriptElement)
    return scriptElement
}


export function addLink(attrs) {
    const link = document.createElement('link')
    for (const attr in attrs) {
        link[attr] = attrs[attr]
    }
    document.head.appendChild(link)
}


export class WebFontLoader {
    constructor(url) {
        this.url = url
        this._resolve = null
    }

    load() {
        this._asyncLoad()
        return new Promise(r => {this._resolve = r})
    }

    cancel() {
        if (this._resolve !== null) {
            this._resolve()
            this._resolve = null
        }
    }

    async _asyncLoad() {
        try {
            const response = await fetch(this.url)
            if (!response.ok) {
                console.log(`Status ${response.status} while loading web fonts from ${this.url}`)
                return
            }

            if (this._resolve === null) {
                // cancel() has been called, so do not fetch the document body
                return
            }

            const responseText = await response.text()

            if (this._resolve === null) {
                // cancel() has been called, so do not insert the CSS
                return
            }

            // Insert the CSS into the DOM
            const element = document.createElement('style')
            element.appendChild(document.createTextNode(responseText))
            document.head.appendChild(element)
        } catch (e) {
            console.log('Error loading web fonts:')
            console.error(e, e.stack)
        } finally {
            this.cancel()
        }
    }
}
